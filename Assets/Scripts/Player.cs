﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Navigator))]
public class Player : MonoBehaviour {
    public static Player player;
    private Navigator nav;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        if(player == null && player != this) player = this;
        else Destroy(this);
        nav = GetComponent<Navigator>();
    }

	// Update is called once per frame
	void Update () {
        MoveByMouse();
	} 

    /// <summary>
    /// If the player clicks, tell the navigator to move us
    /// </summary>
    private void MoveByMouse(){
        if(Input.GetMouseButtonDown(0)){
            nav.NavigateTo(MouseActions.MousePos);
        }
    }
}
