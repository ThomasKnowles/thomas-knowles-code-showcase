﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicEvents : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
		EventManager.RaiseEvent("Update", true);
	}

}
