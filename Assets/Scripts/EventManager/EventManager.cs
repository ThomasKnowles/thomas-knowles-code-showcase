﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;

public static class EventManager {
	// Dictionary to hold events
	private static Dictionary<string, Action> Events = new Dictionary<string, Action>();

	/// <summary>
	/// Static constructor for the event manager, binds into the scene load
	/// so that we can clear the events when the scene starts
	/// </summary>
	static EventManager(){
		// Bind into the scene load delegate
		UnityEngine.SceneManagement.SceneManager.sceneLoaded += SceneLoaded;
	}

	/// <summary>
	/// Called when the scene load
	/// </summary>
	/// <param name="_newScene"></param>
	/// <param name="_mode"></param>
	public static void SceneLoaded(Scene _newScene, LoadSceneMode _mode){
		ResetEvents();
	}

	/// <summary>
	/// Reset the dictionary
	/// </summary>
	public static void ResetEvents(){
		Events.Clear();
	}

	/// <summary>
	/// Adds event to Event listener
	/// </summary>
	/// <param name="_method">Method.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static void AddListener(string _handler, Action _method){
		if (!EventExists (_handler))
			CreateEvent (_handler);

		Events [_handler] += _method;
	}

	/// <summary>
	/// Calls input event
	/// </summary>
	/// <param name="_handler">The name of the event</param>
	/// <param name="ignoreException">If true, no KeyNotFoundException is thrown if the event does not exist</param>
	public static void RaiseEvent(string _handler, bool ignoreException = false){
		if (EventExists (_handler))
			Events [_handler].Invoke ();
		else if (!ignoreException)
			throw new KeyNotFoundException ("No such method exist");
	}

	/// <summary>
	/// Checks if an event exists
	/// </summary>
	/// <returns><c>true</c>, if exists was evented, <c>false</c> otherwise.</returns>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	private static bool EventExists(string _handler){
		return Events.ContainsKey (_handler);
	}

	/// <summary>
	/// Creates the event.
	/// </summary>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	private static void CreateEvent(string _handler){
		Events.Add(_handler, null);
	}
}

