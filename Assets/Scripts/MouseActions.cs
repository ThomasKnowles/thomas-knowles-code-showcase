﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A library with mouse functions
/// </summary>
public class MouseActions {
	public static Vector3 MousePos => m_hit.point;	// An accessable property for the mouse position
	public static GameObject MouseGO => m_hit.transform.gameObject;	// An Accessable property for the gameobject that the ray hits
	static RaycastHit m_hit = new RaycastHit();		// The private storage for the mouse position

	/// <summary>
	/// Static constructor that binds us into the Update methos
	/// </summary>
	static MouseActions() {
		EventManager.AddListener("Update", UpdateMousePos);
	}

	/// <summary>
	/// Called by the update event, simple updates the mouse position in world co-ords
	/// </summary>
	private static void UpdateMousePos(){
		// Gets a ray through the camera
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		// Output of where the ray hits
		RaycastHit hit;
		// If the ray hits something
		if(Physics.Raycast(ray, out hit)){
			m_hit = hit; // The position the ray hit
		}
	}

	/// <summary>
	/// Gets the mouse position at a specific y co-ordinate
	/// </summary>
	/// <param name="y">The y co-ord</param>
	/// <returns>Mouse pos at specific y</returns>
	public static Vector3 MousePosAtY(float y){
		// Locally cache mouse pos so we don't change it
		Vector3 mousePoint = MousePos;
		// Get a line from camera to the point
		Vector3 offset = (mousePoint - Camera.main.transform.position);
		// Get the t offset needed for move up the line
		float t = (y - mousePoint.y) / offset.y;
		// Get the point at the line at specified y
		mousePoint += t * offset;
		return mousePoint;
	}

	/// <summary>
	/// Returns a rotation rotating from the input position to the mouse
	/// </summary>
	/// <param name="pos">position to rotate from</param>
	/// <returns></returns>
	public static Quaternion RotationToMousePos(Vector3 pos){
		// Get the rotation at the vector's y co-ord, and displace
		Vector3 displacement  = MousePosAtY(pos.y) - pos;
		// Return the rotation
		return Quaternion.LookRotation(displacement);
	}
}
