﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MoveLine : MonoBehaviour {

	LineRenderer lr;

	/// <summary>
	/// Awake is called when the script instance is being loaded.
	/// </summary>
	void Awake()
	{
		lr = GetComponent<LineRenderer>();
		lr.textureMode = LineTextureMode.RepeatPerSegment;
	}

	// Update is called once per frame
	void Update () {
		UpdateMoveLine();	
	}

	/// <summary>
	/// Called in update, requests the new path to the mouse
	/// </summary>
	private void UpdateMoveLine(){
		// Make a navigation request from our position to the mouse' position
		NavigationRequest nr = new NavigationRequest (new Navigator.NavInfo (MouseActions.MousePos,
														transform.position), DrawLine);
		nr.FindPath ();
	}

	/// <summary>
	/// Callback from the Navigation system,
	/// Redraws the linerenderer to the new mouse position
	/// </summary>
	/// <param name="_info"></param>
	private void DrawLine(Navigator.NavInfo _info){
		// Unpack the path
		Vector3[] path_segmented = _info.path;

		// Loop through all of the nodes in the path
		if(path_segmented.Length > 0){
			// Ask for a spline for the current path
			CatmullRomSpline cr = new CatmullRomSpline(path_segmented, 0.25f);
			Vector3[] path = cr.Spline.ToArray();
			// Set the amount of segments, and start position
			lr.positionCount = path.Length+1;
			float posY = transform.position.y;
			lr.SetPosition(0, transform.position);
			// Go through all the spline segments to the returned position
			for(int i=0; i<path.Length; i++){
				path[i].y = posY;
				lr.SetPosition(i+1, path[i]);
			}
		}
	}
}
