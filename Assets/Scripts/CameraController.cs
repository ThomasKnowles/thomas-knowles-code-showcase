﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    private GameObject player;
    private Vector3 horizontalDisplacement, verticalDisplacement;
    public float interpolationFactor, horizontalDisplacementMultiplier, verticalDisplacementMultiplier;

	// Use this for initialization
	void Start () {
                player = GameObject.FindGameObjectWithTag("Player");
                horizontalDisplacement = -Vector3.forward;
                verticalDisplacement = Vector3.up;
	}
	
	// Update is called once per frame
	void Update () {

        transform.position = Vector3.Lerp(transform.position, player.transform.position + horizontalDisplacement*horizontalDisplacementMultiplier + verticalDisplacement*verticalDisplacementMultiplier, interpolationFactor);

	}
}
