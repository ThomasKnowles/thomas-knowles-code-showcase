﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


public class GridNavigation : EditorWindow
{

	bool ShowGrid = false;
	enum windowState { OBJECT, BAKE };
	windowState state = windowState.BAKE;

    [SerializeField]
    NavigationData navigationData;
    [SerializeField]
    GameObject navigationDataObject;

    const string NAVIGATION_DATA_PATH = "navigation_tool_data";

    int walkableObjects = 0;
    bool selectionIncludedInSet = false;

    public List<GameObject> SelectedGameObjects = new List<GameObject>();
    
    int gridSize = 1;

    Vector2 scrollVal = Vector3.zero;

    /// <summary>
    /// Window initialisation
    /// </summary>
    [MenuItem("Grid Navigation/Navigation")]
    static void InitialiseWindow()
    {

        GridNavigation navWindow = GetWindow<GridNavigation>("Grid Navigation");
        navWindow.Show();

    }
    

	/// <summary>
	/// Add our OnSceneGUI method to the sceneview OnSceneGUI delegate when the window opened
	/// </summary>
	void OnEnable(){

        //navGridData = (GridNavSO)AssetDatabase.LoadAssetAtPath("Assets/Editor/Navigation/Data/data.asset", typeof(GridNavSO));
        //if(navGridData == null) {
        //    navGridData = CreateInstance<GridNavSO>();
        //    navGridData.SelectedGameObjects = new List<GameObject>();
        //    navGridData.SelectedIDs = new List<int>();
        //    AssetDatabase.CreateAsset(navGridData, "Assets/Editor/Navigation/Data/data.asset");
        //    AssetDatabase.SaveAssets();
        //} else { Debug.Log("Asset Loaded " + navGridData.SelectedIDs.Count); }

        navigationDataObject = GameObject.Find(NAVIGATION_DATA_PATH);
        if (navigationDataObject == null)
        {
            Debug.Log("Navigation data not found, generating new data.");
            navigationDataObject = new GameObject(NAVIGATION_DATA_PATH);
            navigationDataObject.hideFlags = HideFlags.HideInHierarchy;
            navigationData = navigationDataObject.AddComponent<NavigationData>();
        } else { navigationData = navigationDataObject.GetComponent<NavigationData>(); }
        SaveObjectData();

        NavGrid.Load();

        SceneView.onSceneGUIDelegate += OnSceneGUI;
		Selection.selectionChanged += AnalyseSelection;
        EditorApplication.hierarchyWindowChanged += CheckSelections;
	}

	void OnDisable(){
		SceneView.onSceneGUIDelegate -= OnSceneGUI;
		Selection.selectionChanged -= AnalyseSelection;
        EditorApplication.hierarchyWindowChanged -= CheckSelections;
    }


    private void OnGUI()
    {

		GUILayout.BeginHorizontal ();

        GUI.enabled = !(state == windowState.OBJECT);
		if (GUILayout.Button ("Objects", EditorStyles.toolbarButton))
			state = windowState.OBJECT;
        GUI.enabled = !(state == windowState.BAKE);
        if (GUILayout.Button ("Bake", EditorStyles.toolbarButton))
			state = windowState.BAKE;
        GUI.enabled = true;

		GUILayout.EndHorizontal ();
        EditorGUILayout.Space();

		switch (state) {
		case windowState.BAKE:
			BakeMenu ();
			break;
		case windowState.OBJECT:
			ObjectMenu ();
			break;

		default:
			BakeMenu ();
			break;
		}

    }

	private void BakeMenu(){

        GUILayout.Label("Navigation Grid", EditorStyles.boldLabel);
        gridSize = EditorGUILayout.IntField("Grid Size", gridSize);
        GUILayout.BeginHorizontal();
        if (NavGrid.Grid == null ||  NavGrid.grids <= 0)
        {
            if (GUILayout.Button("Bake Grid")) { NavGrid.Bake(navigationData.SelectedGameObjects, gridSize); SceneView.RepaintAll(); }    // Show button option for baking navigation grid
            if (GUILayout.Button("Load Grid")) { NavGrid.Load(); SceneView.RepaintAll(); }       // Show button option for loading navigation grid
        } else {
            if (GUILayout.Button("Rebake Grid")) { NavGrid.Bake(navigationData.SelectedGameObjects, gridSize); SceneView.RepaintAll(); }  // Show button option for rebaking navigation grid
            if (GUILayout.Button("Load Grid")) { NavGrid.Load(); SceneView.RepaintAll(); }       // Show button option for reloading navigation grid
        }
        GUI.enabled = NavGrid.grids <= 0 ? false : true;
        if (GUILayout.Button("Clear Grid")) { NavGrid.Clear(); SceneView.RepaintAll(); }
        GUILayout.EndHorizontal();

        // Show button options for showing and hiding sceneview representation of grid map
        GUI.enabled = NavGrid.grids <= 0 ? false : true;
        if (ShowGrid)
        {
            if (GUILayout.Button("Hide Grid")) { ShowGrid = false; SceneView.RepaintAll(); }
        }
        else if (GUILayout.Button("Show Grid")) { ShowGrid = true; SceneView.RepaintAll(); }
        GUI.enabled = true;

    }

	private void ObjectMenu(){
        GUILayout.Label("Walkable Selections", EditorStyles.boldLabel);

        if (GUILayout.Button("Add selection to walkable"))
        {
            AddSelection();
        }
        GUI.enabled = selectionIncludedInSet;
        if(GUILayout.Button("Remove selection from walkable"))
        {
            RemoveSelection();
        }
        GUI.enabled = true;

        scrollVal = EditorGUILayout.BeginScrollView(scrollVal);

        IEnumerator enumerable = navigationData.SelectedGameObjects.GetEnumerator();
        while (enumerable.MoveNext())
        {
            Object o = (Object)enumerable.Current;
            GUILayout.BeginHorizontal();
            o = EditorGUILayout.ObjectField(o, typeof(GameObject), true);

            if (GUILayout.Button("-"))
            {
                RemoveSelection((GameObject)o);
                AnalyseSelection();
                break;
            }

            GUILayout.EndHorizontal();

        }

        EditorGUILayout.EndScrollView();


    }

	private void AnalyseSelection(){
        selectionIncludedInSet = false;
		foreach(GameObject g in Selection.gameObjects)
        {
            if (navigationData.SelectedGameObjects.Contains(g))
            {
                selectionIncludedInSet = true;
                break;
            }
        }
        Repaint();

    }

    private void AddSelection()
    {
        foreach (GameObject g in Selection.gameObjects)
        {
            navigationData.SelectedGameObjects.Add(g);
            walkableObjects++;
        }
        AnalyseSelection();
        SaveObjectData();
        Repaint();
    }
    private void RemoveSelection()
    {
        foreach (GameObject g in Selection.gameObjects)
        {
            navigationData.SelectedGameObjects.Remove(g);
        }
        AnalyseSelection();
        SaveObjectData();
        Repaint();
    }

    private void RemoveSelection(GameObject g)
    {
        navigationData.SelectedGameObjects.Remove(g);
        AnalyseSelection();
        SaveObjectData();
        Repaint();
    }

    private void CheckSelections()
    {
        navigationData.SelectedGameObjects.RemoveAll(x => x == null);
        AnalyseSelection();
        SaveObjectData();
        Repaint();
    }
    
    private void SaveObjectData()
    {
		if (!Application.isPlaying) {
			UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty (UnityEditor.SceneManagement.EditorSceneManager.GetActiveScene ());
		}
    }

	void OnSceneGUI(SceneView sceneView){
		if (Event.current.type == EventType.Repaint) {
			if(ShowGrid && NavGrid.grids > 0){
				foreach (Node n in NavGrid.Grid.Values) {
					Handles.color = n.walkable ? Color.green : Color.red;
					Handles.DotHandleCap (
						0,
						n.worldPosition,
						Quaternion.identity,
						.4f,
						EventType.Repaint
					);
				}
			}
		}
	}
}
