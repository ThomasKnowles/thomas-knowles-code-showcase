﻿using System.Collections.Generic;
using UnityEngine;

[System.Obsolete]
[System.Serializable]
public class GridNavSO : ScriptableObject
{
    [System.NonSerialized]
    public List<GameObject> SelectedGameObjects = new List<GameObject>();

    [SerializeField]
    public List<int> SelectedIDs;

    void OnEnable()
    {
        Debug.Log("EnableCalled");
        try
        {
            if (SelectedIDs.Count > 0)
            {
                foreach (int i in SelectedIDs)
                {
                    SelectedGameObjects.Add((GameObject)UnityEditor.EditorUtility.InstanceIDToObject(i));
                }
            }
        }
        catch (System.Exception e) { Debug.LogError("Loading selected IDs failed: " + e.Message); }
    }

    public void Add(GameObject g)
    {
        SelectedGameObjects.Add(g);
        SelectedIDs.Add(g.GetInstanceID());
    }

    public void Remove(GameObject g)
    {
        SelectedGameObjects.Remove(g);
        SelectedIDs.Remove(g.GetInstanceID());
    }

    public void RemoveAll(System.Predicate<GameObject> match)
    {
        List<GameObject> ToDelete = SelectedGameObjects.FindAll(match);
        System.Collections.IEnumerator enumerable = ToDelete.GetEnumerator();
        foreach (GameObject g in ToDelete)
        {
            SelectedIDs.Remove(g.GetInstanceID());
            SelectedGameObjects.Remove(g);
        }
    }

}
