﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class NavigationRequest {
	public static bool initialised = false;
	Navigator.NavInfo info;
	Action<Navigator.NavInfo> ReturnTo;

	public NavigationRequest(Navigator.NavInfo _info, Action<Navigator.NavInfo> _returnTo){
		info = _info;
		ReturnTo = _returnTo;
	}

	/// <summary>
	/// Uses the navigation flags to choose which navigation algorithm to use
	/// </summary>
	/// <param name="info">Navigation info struct that holds all needed info</param>
	/// <param name="ReturnTo">Action that holds a delegate that accepts a navinfo struct to pass back to navigator component</param>
	public void FindPath()
	{
		if (info.navFlags == Navigator.NavigationFlags.Accurate)
			FindPath_AStar ();
		else
			FindPath_Greedy ();

	}

    /// <summary>
    /// Request that Navigation finds a path. Uses A* to search for the shortest path between startPos and targetPos. After path is found, ReturnTo is called using path as a parameter
    /// </summary>
    /// <param name="startPos">Position to path from</param>
    /// <param name="targetPos">Position to path to</param>
    /// <param name="ReturnTo">Method called after path is found</param>
	void FindPath_AStar()
	{
		Vector3 startPos = info.startPos;
		Vector3 targetPos = info.targetPos;

		try
		{
			// Instantiate nodes that correlate to input positions
			Node startNode = NavGrid.NodeFromWorldPoint(startPos);
			Node targetNode = NavGrid.NodeFromWorldPoint(targetPos);

			// Set up a heap and hashset to contain the open and closed sets
			Heap<Node> openSet = new Heap<Node>(NavGrid.grids);
			HashSet<Node> closetSet = new HashSet<Node>();

			// Add the first node to the open set
			openSet.Add(startNode);
			openSet.UpdateItem(startNode);

			// Loop while there are still nodes in the open set
			while (openSet.Count > 0)
			{
				// Pop the first node off the open set to be analysed
				Node currentNode = openSet.removeFirst();

				// Add the first node to te closed set so it is not looked at
				closetSet.Add(currentNode);

				// If we have reached to target node, return the path using ReturnTo
				if (currentNode.worldPosition == targetNode.worldPosition)
				{
					info.path = RetracePath(startNode, targetNode);
					ReturnTo(info);
					return;
				}

				List<Node> neighbours = new List<Node>(currentNode.neighbours);
				// Check the neighbours
				foreach (Node neighbour in neighbours)
				{
					if(neighbour == null) continue;

					// If the neighbour cannot be pathed to, or the neighbour has already been computed, then move on to next neighbour
					if (!neighbour.walkable || closetSet.Contains(neighbour)) continue;

					// Calculate the movement cost to the neighbour from current node
					int newMovementCostToNeighbour = currentNode.gCost + GetDistance(currentNode, neighbour);
					// if the new movement is cheaper, or the neighbour costs haven't been calculated yet
					if (newMovementCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour))
					{
						// Calculate the g, h, (and thus the f,) costs of this neighbour
						neighbour.gCost = newMovementCostToNeighbour;
						neighbour.hCost = GetDistance(neighbour, targetNode);

						// Set this neighbour's parent node to be the current node
						neighbour.parent = currentNode;

						// if neighbour isn't in the openset, add to the open set
						if (!openSet.Contains(neighbour)){
							openSet.Add(neighbour);
							openSet.UpdateItem(neighbour);
						}
						// if the item IS in the neighbour, update its position in the heap
						else
							openSet.UpdateItem(neighbour);
					}

				}
			}
		} catch (Exception e)
		{
			Debug.LogError(e);
			System.Threading.Thread.CurrentThread.Abort ();
		}
	}

	/// <summary>
	/// Grabs children for greedy pathfinding
	/// </summary>
	/// <returns>A sorted list of nodes based on the input root and target nodes</returns>
	/// <param name="_root">Node to calculate children from</param>
	/// <param name="_target">Pathfinding target</param>
	List<Node> Greedy_HeuristicSort(Node _root, Node _target)
    {
		// Extract children using linq 
		var childrenSequence = 
			from node in _root.GetNeighbours ()
			orderby GetDistance(node, _target) ascending
			select node;

		// Create a list
		List<Node> children = new List<Node> ();

		// Extract ordered sequence into list
		foreach (Node n in childrenSequence) {
			if (n.walkable)
				children.Add (n);
		}

		// Return sorted list of children
		return children;
    }

	int j = 0;
	void FindPath_Greedy(){
		try{
			// Extract info from struct for readability
			Node startNode = NavGrid.NodeFromWorldPoint(info.startPos);
			Node targetNode = NavGrid.NodeFromWorldPoint(info.targetPos);

			// Get list of nodes from greedy search and reverse their order
				List<Node> path = GreedyPath (startNode, targetNode, new List<Node>(), new HashSet<Node>());
			path.Reverse ();
			Debug.Log (j + "Checks, path size: " + path.Count);

			List<Vector3> vectorPath = new List<Vector3>();
			foreach(Node n in path) vectorPath.Add(n.worldPosition);

			Debug.Log (vectorPath.Count);

			// Cast the path to an array, and add to the struct
			info.path = vectorPath.ToArray();
			// Call the returnto method with the struct
			ReturnTo(info);
		}catch (Exception e){
			Debug.LogError (e);
		}

	}

	List<Node> GreedyPath(Node _root, Node _target, List<Node> _path, HashSet<Node> _painted)
    {
		j++;
		//Sort Children according to Heuristic Function
		List<Node> children = Greedy_HeuristicSort(_root, _target);
		// Loop through the children
		for(int i = 0; i< children.Count; i++){
			
			//if the child has not been painted
			if(!_painted.Contains(children[i])){
				//paint the child node
				_painted.Add(children[i]);
				//if we reached the goal node
				if(children[i] == _target){
					//add node to path
					_path.Add(children[i]);
					//return path
					return _path;
				}
				//Recurse the search on the child node
				_path = GreedyPath(children[i], _target, _path, _painted);

				//We have a path, wind back through path
				if (_path.Count > 0) {
					_path.Add (children[i]);
					return _path;
				}
			}
		}

		return _path;

    }
    

	/// <summary>
	/// Called during pathfinding. This function takes calculates the path based on the set parents, and returns it as a Vector array
	/// </summary>
	/// <param name="startNode"></param>
	/// <param name="endNode"></param>
	/// <returns></returns>
	Vector3[] RetracePath(Node startNode, Node endNode)
	{
		// List to contain path
		List<Vector3> path = new List<Vector3>();
		// Node we're looking at (start at end and work inwards)
		Node currentNode = endNode;

		// Loop through parents until we arrive at the starting node
		while(currentNode != startNode)
		{
			// Add the current node to path list
			path.Add(currentNode.worldPosition);
			// Tell next loop to look at parent
			currentNode = currentNode.parent;
		}
		path.Add(startNode.worldPosition);
		// Reverse the path because it is calculated backwards
		path.Reverse();

		// Return the path as a Vector array
		return path.ToArray();

	}

	/// <summary>
	/// Returns the manhatten distance between two nodes
	/// </summary>
	/// <param name="nodeA">Starting Node</param>
	/// <param name="nodeB">End Node</param>
	/// <returns>Manhatten distance between nodeA and nodeB</returns>
	int GetDistance(Node nodeA, Node nodeB)
	{
		int x_offset = Mathf.Abs((int)nodeA.worldPosition.x - (int)nodeB.worldPosition.x);
		int y_offset = Mathf.Abs((int)nodeA.worldPosition.z - (int)nodeB.worldPosition.z);

		if(x_offset > y_offset)
			return 14 * y_offset + 10 * (x_offset - y_offset);
		return 14 * x_offset + 10 * (y_offset - x_offset);
	}
}