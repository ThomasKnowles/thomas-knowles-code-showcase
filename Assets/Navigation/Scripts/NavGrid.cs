﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;
using System.Linq;

public static class NavGrid {

    private static string SERIALIZE_PATH = "NavGridData.bin";
	public static LayerMask unwalkableMask = 0;

	public static bool initialised = false;

	private static float _nodeRadius = .5f;

	private static GridGraph m_graph;
	public static int GridSize { get { return m_graph.GridSize; } }
	public static Hashtable Grid { get{ return m_graph.m_graph; } }

	private static float nodeDiameter = 1f;
	private static int gridSizeX = 0, gridSizeY = 0;

	public static List<Vector3> path;

    private static string NAVIGATION_DATA_PATH = "navigation_tool_data";

    public static int grids
	{
		get { if(Grid != null) return Grid.Count;
				else return 0; }
	}

	static NavGrid(){
		DeSerialiseNavGrid();
	}

	/// <summary>
	/// Used by navigation tool to bake the 
	/// </summary>
	public static void Bake(List<GameObject> walkableSet, int gridSize)
    {

        Clear();
		GenerateNavGrid(walkableSet, gridSize);
		SerialiseNavGrid ();
	}

    public static void Clear()
    {
        initialised = false;
		m_graph = new GridGraph();
    }
    public static void Load()
    {
        DeSerialiseNavGrid();
    }

    /// <summary>
    /// was used while trying to use unity serialisation in a gameobject to store hashtable
    /// </summary>
    [System.Obsolete]
    public static void Save()
    {
        //GameObject navDataObject = GameObject.Find(NAVIGATION_DATA_PATH);
        //NavigationData navData = navDataObject.GetComponent<NavigationData>();
        //if (grid.Count > 0) { navData.grid = grid; Debug.Log("GridMap successfully saved"); }
        //else { Debug.Log("GridMap is null, cannot be saved"); }

        //UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(UnityEditor.SceneManagement.EditorSceneManager.GetActiveScene());

        //Debug.Log(navData.grid.Count);
    }

    /// <summary>
    /// Generates the navigation grid based on supplied walkable geometry
    /// </summary>
	static void GenerateNavGrid(List<GameObject> walkableSet, int gridSize)
	{
		m_graph = new GridGraph(gridSize);
		_nodeRadius = gridSize/2;
		nodeDiameter = gridSize;

		//GameObject[] WorldGeometry = GameObject.FindGameObjectsWithTag("Walkable");
		foreach (GameObject WalkableArea in walkableSet)
		{
			Renderer renderer = WalkableArea.GetComponent<MeshRenderer>();
			Vector3 boundingSize = renderer.bounds.size;

			Vector3 worldBottomLeft = WalkableArea.transform.position - Vector3.right * boundingSize.x / 2 - Vector3.forward * boundingSize.z / 2;
			worldBottomLeft += Vector3.up;
			worldBottomLeft = RoundVectorToNearestX(gridSize, worldBottomLeft);

			gridSizeX = Mathf.RoundToInt(boundingSize.x / nodeDiameter);
			gridSizeY = Mathf.RoundToInt(boundingSize.z / nodeDiameter);

			for (int x = 0; x < gridSizeX; x++)
			{
				for (int y = 0; y < gridSizeY; y++)
				{
					Vector3 worldPoint = worldBottomLeft + Vector3.right * (x * nodeDiameter) + Vector3.forward * (y * nodeDiameter);
					RoundVectorToInt(ref worldPoint);
					int doesNotAffectMask = ~(1 << 9); 
					bool walkable = !(Array.Exists(Physics.OverlapSphere(worldPoint, _nodeRadius, doesNotAffectMask), element => walkableSet.Contains(element.gameObject) == false));

					AddNode(worldPoint, walkable);
				}
			}
		}
		SetNeighbours();
        initialised = true;
    }

    /// <summary>
    /// Used to serialise navigation data to a binary file
    /// </summary>
	static void SerialiseNavGrid(){
        // If the grid data isn't null, and therefor can be serialised
        if(Grid !=null){
            // Create a new filestream pointing to the default application data path, create file if not made, write-only acess
            Stream stream = new FileStream(Path.Combine(Application.dataPath, SERIALIZE_PATH), FileMode.Create, FileAccess.Write, FileShare.None);
            // Try block to catch unexpected errors
            try
            {
                // Create the binary formatter used to convert data to a bit stream
                BinaryFormatter formatter = new BinaryFormatter();

                // Create a surrogate sellected and add to the binary formatter so that .net understands how to serialse Vector3 classes
                SurrogateSelector ss = new SurrogateSelector();
                Vector3SerializationSurrogate v3ss = new Vector3SerializationSurrogate();
                ss.AddSurrogate(typeof(Vector3),new StreamingContext(StreamingContextStates.All),v3ss);
                formatter.SurrogateSelector = ss;

                // Serialise the information into binary format
                formatter.Serialize(stream, m_graph);
            }
            // Occurs if there is a problem such as an unserialiseable datatype marked to be serialised
            catch (SerializationException e)
            {
                Debug.LogError("Serialization Failed: " + e.Message);
            }
            // Make sure to close the file stream once everything has been attempted to avoid memory leaks
            finally
            {
                stream.Close();
            }
        // Debug an error to unity if there is no data to be serialised
        }else Debug.LogError("Cannot serialize null navgrid!");

	}

    /// <summary>
    /// Used to deserialise navigation data to a binary file
    /// </summary>
    static void DeSerialiseNavGrid()
    {
        // Create a new filestream pointing to the default application data path, only open the file, read-only acess
        Stream stream = new FileStream(Path.Combine(Application.dataPath, SERIALIZE_PATH), FileMode.Open, FileAccess.Read, FileShare.Read);
        try
        {
            // Create the binary formatter used to convert data to a bit stream
            BinaryFormatter formatter = new BinaryFormatter();

            // Create a surrogate sellected and add to the binary formatter so that .net understands how to serialse Vector3 classes
            SurrogateSelector ss = new SurrogateSelector();
			Vector3SerializationSurrogate v3ss = new Vector3SerializationSurrogate();
			ss.AddSurrogate(typeof(Vector3),new StreamingContext(StreamingContextStates.All),v3ss);
			formatter.SurrogateSelector = ss;

            // Deserialise into managed code
            m_graph = (GridGraph)formatter.Deserialize(stream);
        }
        // Occurs if there is a problem such as an unserialiseable datatype marked to be serialised
        catch (SerializationException e) { Debug.LogError("Serialization failed: " + e.Message); }
        // Occurs if there is a problem loading the file such as no file exists
        catch (IOException e) { Debug.LogError("FileStream failed: " + e.Message);}
        // Make sure to close the file stream once everything has been attempted to avoid memory leaks
        finally
        {
            stream.Close();
        }
    }

	/// <summary>
	/// Adds a node to the grid
	/// </summary>
	/// <param name="_worldPoint">The point at which to add the node</param>
	/// <param name="_walkable"> whether the node should be blocked</param>
	private static void AddNode(Vector3 _worldPoint, bool _walkable)
	{
        bool areNodesOverlapped = false;
		if (!Grid.ContainsKey((_worldPoint.x + "," + _worldPoint.z)))
			Grid.Add((_worldPoint.x + "," + _worldPoint.z), new Node(_walkable, _worldPoint));
		else
            areNodesOverlapped = true;
		// Tell us that nodes are overlapped when baking
        if (areNodesOverlapped) Debug.Log("Some nodes are overlapped");
	}

	/// <summary>
	/// Converts a world position to a node
	/// </summary>
	/// <param name="worldPosition"></param>
	/// <returns></returns>
	public static Node NodeFromWorldPoint(Vector3 worldPosition)
	{
		// Get the closest node
		worldPosition = RoundVectorToNearestX(GridSize, worldPosition);
		if (Grid.ContainsKey((worldPosition.x + "," + worldPosition.z)))
			return Grid[worldPosition.x + "," + worldPosition.z] as Node;
		else
		{
			// Tell the dev that the world position doesn't exist
			throw new NullReferenceException ("Invalid world position " + worldPosition + ": node does not exist");
		}
	}

	/// <summary>
	/// Rounds each vector component to the nearest multiple of x
	/// </summary>
	/// <param name="roundTo">The number to round each component to</param>
	/// <param name="vec">The input vector</param>
	/// <returns></returns>
	public static Vector3 RoundVectorToNearestX(int roundTo, Vector3 vec){
		vec.x = roundFloatToNearestX(roundTo, vec.x);
		vec.y = roundFloatToNearestX(roundTo, vec.y);
		vec.z = roundFloatToNearestX(roundTo, vec.z);
		return vec;
	}
	/// <summary>
	/// Rounds a float to the nearest multiple of x
	/// </summary>
	/// <param name="roundTo">The number to round to</param>
	/// <param name="f"></param>
	/// <returns></returns>
	static float roundFloatToNearestX(int roundTo, float f){
		return Mathf.Round(f/roundTo)*roundTo;
	}

	/// <summary>
	/// Rounds a vector to the closest integer
	/// </summary>
	/// <param name="vec">Refernce to the vector</param>
	[System.Obsolete]
	public static void RoundVectorToInt(ref Vector3 vec)
	{
		vec.x = Mathf.RoundToInt(vec.x);
		vec.y = Mathf.RoundToInt(vec.y);
		vec.z = Mathf.RoundToInt(vec.z);
	}

	/// <summary>
	/// Returns node at specific points
	/// </summary>
	/// <returns>The node.</returns>
	/// <param name="x">The x coordinate.</param>
	/// <param name="z">The z coordinate.</param>
	public static Node GetNode (int x, int z){
		return Grid [x + "," + z] as Node;
	}

	/// <summary>
	/// Does a node exist at point?
	/// </summary>
	/// <returns>Whether a node exists at a point</returns>
	/// <param name="x">The x coordinate.</param>
	/// <param name="z">The z coordinate.</param>
	public static bool ExistsNode(int x, int z){
		return Grid.ContainsKey (x + "," + z);
	}

    /// <summary>
    /// Checks if there are nodes adjacent to the current node, if yes, set those nodes as neighbours
    /// </summary>
	public static void SetNeighbours()
	{
		foreach (var key in Grid.Keys)
		{
			// get the current node
			Node n = Grid[key] as Node;
			// Store world position for ease and readability
			Vector3 wp = n.worldPosition;

			if (ExistsNode ((int)wp.x-GridSize, (int)wp.z)) {
				GetNode ((int)wp.x-GridSize, (int)wp.z).neighbours.Add(n);
			}
			if (ExistsNode ((int)wp.x+GridSize, (int)wp.z)) {
				GetNode ((int)wp.x+GridSize, (int)wp.z).neighbours.Add(n);
			}
			if (ExistsNode ((int)wp.z, (int)wp.z-GridSize)) {
				GetNode ((int)wp.x, (int)wp.z-GridSize).neighbours.Add(n);
			}
			if (ExistsNode ((int)wp.z, (int)wp.z+GridSize)) {
				GetNode ((int)wp.x, (int)wp.z+GridSize).neighbours.Add(n);
			}

		}
	}

	// *****************************
	// Set neighbours with diagonals
	// *****************************
	// public static void SetNeighbours()
	// {
	// 	foreach (var key in Grid.Keys)
	// 	{
	// 		// get the current node
	// 		Node n = Grid[key] as Node;
	// 		// Store world position for ease and readability
	// 		Vector3 wp = n.worldPosition;

	// 		// Loop through the neighbour positions and add them if they exist
	// 		for(int i = (int)wp.x-GridSize; i <= (int)wp.x+GridSize; i+=GridSize){
	// 			for(int j = (int)wp.z-GridSize; j <= (int)wp.z+GridSize; j+=GridSize){
	// 				if (!(i == wp.x && j == wp.z)) {
	// 					if (ExistsNode (i, j)) {
	// 						GetNode (i, j).neighbours.Add(n);
	// 					}
	// 				}

	// 			}
	// 		}
	// 	}
	// }

	/// <summary>
	/// Holds data to be serialized
	/// </summary>
	[Serializable]
	public struct GridGraph{
		int m_gridSize;
		public int GridSize { get { return m_gridSize;} }
		public Hashtable m_graph;

		public GridGraph(int _gridSize){
			m_graph = new Hashtable();
			m_gridSize = _gridSize;
		}
	}
}