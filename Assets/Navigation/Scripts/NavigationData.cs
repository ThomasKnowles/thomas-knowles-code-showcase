﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

/// <summary>
/// Holds the selected game objects
/// </summary>
[System.Serializable]
public class NavigationData : MonoBehaviour
{
    [SerializeField]
    public List<GameObject> SelectedGameObjects = new List<GameObject>();
    
}