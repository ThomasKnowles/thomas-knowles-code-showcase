﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Heap<T> where T : IHeapItem<T> {
	T[] items;					// The array to contain the heap items
	int currentItemCount;		// The amount of items in the heap

	/// <summary>
	/// Constuctor for the heap
	/// </summary>
	/// <param name="maxHeapSize">The maximum amount of items in the heap</param>
	public Heap(int maxHeapSize)
	{
		items = new T[maxHeapSize];
	}

	/// <summary>
	/// Adds a new item to the heap
	/// </summary>
	/// <param name="item">The T item to add</param>
	public void Add(T item)
	{
		item.HeapIndex = currentItemCount;
		items[currentItemCount] = item;
		SortUp(item);
		currentItemCount++;
	}

	/// <summary>
	/// Removes the first item from the heap
	/// </summary>
	/// <returns>The first item from the heap</returns>
	public T removeFirst()
	{
		// if there's enough items in to remove one
		if (currentItemCount > 0) {
			T firstItem = items [0];
			currentItemCount--;
			items [0] = items [currentItemCount];
			items [0].HeapIndex = 0;
			SortDown (items [0]);
			return firstItem;
		} else
			throw new IndexOutOfRangeException ("Cannot remove items, no items to remove");
	}

	/// <summary>
	/// Returns whether the heap contains an item
	/// </summary>
	/// <param name="item"></param>
	/// <returns></returns>
	public bool Contains(T item)
	{
		return Equals(items[item.HeapIndex], item);
	}

	/// <summary>
	/// Sorts the items because something's been added or removed
	/// </summary>
	/// <param name="item"></param>
	public void UpdateItem(T item)
	{
		SortUp(item);
	}

	/// <summary>
	/// Returns how many items are in the heap
	/// </summary>
	/// <returns></returns>
	public int Count
	{
		get { return currentItemCount; }
	}

	/// <summary>
	/// Sorts the items downwards
	/// </summary>
	/// <param name="item"></param>
	void SortDown(T item)
	{
		// We know this will never be an infinate loop
		while (true)
		{
			// Formulas for the left and right children
			int childIndexLeft = item.HeapIndex * 2 + 1;
			int childIndexRight = item.HeapIndex * 2 + 2;
			int swapIndex = 0;

			// If we haven't hit the end
			if (childIndexLeft < currentItemCount)
			{
				swapIndex = childIndexLeft;

				// If we haven't hit the end
				if (childIndexRight < currentItemCount)
				{
					// Whether the left is less than the right
					if (items[childIndexLeft].CompareTo(items[childIndexRight]) < 0)
					{
						swapIndex = childIndexRight;
					}
				}

				// If the current item is less then the swap item
				if (item.CompareTo(items[swapIndex]) < 0)
				{
					Swap(item, items[swapIndex]);
				}
				else return;

			}
			else return;

		}
	}

	/// <summary>
	/// Sorts the heap upwards
	/// </summary>
	/// <param name="item"></param>
	void SortUp(T item)
	{
		// The item to sawp
		int parentIndex = (item.HeapIndex - 1) / 2;

		while (true)
		{
			T parentItem = items [parentIndex];
			if (item.CompareTo (parentItem) > 0) {
				Swap (item, parentItem);
			} else
				break;

			parentIndex = (item.HeapIndex - 1) / 2;
		}
	}

	/// <summary>
	/// Swap two items
	/// </summary>
	/// <param name="itemA"></param>
	/// <param name="itemB"></param>
	void Swap(T itemA, T itemB)
	{
		items[itemA.HeapIndex] = itemB;
		items[itemB.HeapIndex] = itemA;
		int itemAIndex = itemA.HeapIndex;
		itemA.HeapIndex = itemB.HeapIndex;
		itemB.HeapIndex = itemAIndex;

	}

}

/// <summary>
/// An interfact to keep heapitems
/// </summary>
/// <typeparam name="T"></typeparam>
public interface IHeapItem<T>: IComparable<T>{
	int HeapIndex
	{
		get;
		set;
	}
}