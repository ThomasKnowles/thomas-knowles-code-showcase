﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using System;

[RequireComponent(typeof(CharacterController))]
public class Navigator : MonoBehaviour {

	[Range(0, 2)]
	public float stepSpeed = 0.5f;
	public float stepDelay = 1f;
	[Range(0,1)]
	public float rotateSpeed = 0.1f;
	[Range(0,0.2f)]
	public float stepDistanceThreashold = 0.1f;

	[Range(0f,1f)]
	public float m_moveSpeed = 1f;

	Vector3 targetDestination;
	Vector3[] curPath;

	public Transform target;
	Thread RequestPathToDestination;
	Queue<NavInfo> pathQueue = new Queue<NavInfo>();
	CharacterController controller;

	public enum NavigationFlags { Fast, Accurate };
	public enum MovementMode { Smooth, Hop };

	void Update()
	{
		if(pathQueue.Count > 0)
		{
			PathTo(pathQueue.Dequeue());
		}
	}

	/// <summary>
	/// Path gameObject to position
	/// </summary>
	/// <param name="_targetPosition">The position to path to</param>
	public void NavigateTo(Vector3 _targetPosition, NavigationFlags _navigationFlags = NavigationFlags.Accurate, MovementMode _movementMode = MovementMode.Smooth)
	{
		Vector3 pos = transform.position;
		ThreadStart ts = delegate {
			NavigationRequest nr = new NavigationRequest (new NavInfo (_targetPosition, pos, _navigationFlags, _movementMode), CallBack);
			nr.FindPath ();
		};
		ts.Invoke ();

	}

	/// <summary>
	/// Callback from Navigation class. Adds the requested path to the event queue
	/// </summary>
	/// <param name="path"></param>
	void CallBack(NavInfo _info)
	{
		pathQueue.Enqueue(_info);
	}

	/// <summary>
	/// Called when there's a navigation event to pick the 
	/// correct movement algorithm based on the input
	/// movement mode
	/// </summary>
	/// <param name="_info"></param>
	void PathTo(NavInfo _info)
	{
		
		if (_info.moveFlags == MovementMode.Hop) {
			StopCoroutine("MoveTo_Hop");
			StartCoroutine ("MoveTo_Hop", _info.path);
		}

		if (_info.moveFlags == MovementMode.Smooth) {
			StopCoroutine("MoveTo_Smooth");
			StartCoroutine ("MoveTo_Smooth", _info.path);
		}
	}

	/// <summary>
	/// Uses a chess board movement style
	/// </summary>
	/// <param name="_path"></param>
	/// <returns></returns>
	IEnumerator MoveTo_Hop(Vector3[] _path)
	{
		for (int i = 0; i < _path.Length; i++) {

			Vector3 pos = transform.position;
			float j = 0;
			do {
				pos = Vector3.Lerp(pos, _path[i], Mathf.SmoothStep(0,1,j/m_moveSpeed));
				transform.position = pos;
				j += Time.deltaTime;
				yield return null;
			} while (j < m_moveSpeed);

		}
	}

	/// <summary>
	/// Moves in a smooth style
	/// </summary>
	/// <param name="_path"></param>
	/// <returns></returns>
	IEnumerator MoveTo_Smooth(Vector3[] _path){
		Vector3 pos;
		foreach (Vector3 curNode in _path) {
			do {
				pos = transform.position;
				pos.y = curNode.y;
				Vector3 moveTowards = (curNode - transform.position).normalized * m_moveSpeed;
				controller.Move(moveTowards);
				yield return null;
			} while (Vector3.Distance (curNode, pos) > stepDistanceThreashold);
		}

	}

	/// <summary>
	/// Is value x within distance y of value z
	/// </summary>
	/// <param name="floatX">First value to check</param>
	/// <param name="floatZ">Second value to check</param>
	/// <param name="distanceY">threashold distance [inclusive] e.g. is 5 within 1 of 6? Answer yes</param>
	/// <returns></returns>
	bool twoFloatsWithinDistance(float floatX, float floatZ, float distanceY)
	{
		return Mathf.Abs(floatX - floatZ) >= distanceY;
	}

	/// <summary>
	/// Struct to easily pass all the info needed throughout the navigation process
	/// </summary>
	public struct NavInfo{

		public Vector3 targetPos;
		public Vector3 startPos;
		public Vector3[] path;
		public NavigationFlags navFlags;
		public MovementMode moveFlags;

		public NavInfo( Vector3 _targetPos, Vector3 _startPos, 
						NavigationFlags _navFlags = NavigationFlags.Accurate,
						 MovementMode _movementFlags = MovementMode.Smooth,
						  Vector3[] _path = default(Vector3[])){
			targetPos = _targetPos;
			startPos = _startPos;
			path = _path;
			navFlags = _navFlags;
			moveFlags = _movementFlags;
		}

	}

	/// <summary>
	/// Awake is called when the script instance is being loaded.
	/// </summary>
	void Awake()
	{
		controller = GetComponent<CharacterController>();
	}

}