﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Node : IHeapItem<Node> {

	public bool walkable = false;                		// Can this node be navigated to?
	public Vector3 worldPosition = Vector3.zero;       	// The position of this Node
	public int gCost = 0;                    			// Distance from parent node
	public int hCost = 0;            					// Hollistic distance to target
	public List<Node> neighbours = new List<Node>();    // Nodes around current node
	public Node parent;                                 // Parent Node
	int heapIndex;                                      // Index in heap

	/// <summary>
	/// Fcost for A*, returns combination of h, and g costs
	/// </summary>
	public int fCost
	{
		get { return gCost + hCost; }
	}

	/// <summary>
	/// Neighbours of this node
	/// </summary>
	/// <returns>List of neighbours</returns>
	public List<Node> GetNeighbours()
	{
		return neighbours;
	}


	/// <summary>
	/// Constructor, sets up whether this node is walkable, and the position of this node
	/// </summary>
	/// <param name="_walkable">Can this node be walked on?</param>
	/// <param name="_worldPos">Where this node is</param>
	public Node(bool _walkable, Vector3 _worldPos) {
		walkable = _walkable;
		worldPosition = _worldPos;
	}

	/// <summary>
	/// Interface implementation, the index this node is at on the heap
	/// </summary>
	public int HeapIndex
	{
		get { return heapIndex; }
		set { heapIndex = value; }
	}

	/// <summary>
	/// Interface implementation, compares nodes based on fCost, if fCost is same, pick lowest hCost
	/// </summary>
	/// <param name="_node"></param>
	/// <returns></returns>
	public int CompareTo(Node _node)
	{
		int compare = fCost.CompareTo(_node.fCost);
		if(compare == 0)
		{
			compare = hCost.CompareTo(_node.hCost);
		}

		return -compare;
	}

}