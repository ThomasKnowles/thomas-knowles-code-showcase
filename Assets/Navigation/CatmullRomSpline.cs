﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatmullRomSpline {
    Vector3[] linePoints;       // Points in the line
    float resolution = 0.02f;   // 0 < Resolution <= 1. Must evenly add to 1

    private List<Vector3> spline = new List<Vector3>();                                 // The actual spline being calculated
    public List<Vector3> Spline { get {return spline;} private set{spline = value;} }   // Property for spline

    /// <summary>
    /// Constructor for the spline
    /// </summary>
    /// <param name="_line">The amount of points to construct the line</param>
    /// <param name="_res">How many segments per point</param>
    public CatmullRomSpline(Vector3[] _line, float _res) {
        linePoints = _line;
        resolution = _res;
        SetSpline();
    }

    /// <summary>
    /// Creates the spline
    /// </summary>
    private void SetSpline(){
        Spline.Add(linePoints[0]);
        int maxLen = linePoints.Length-1;
        // Go through all of the points on the line, and creats spline points based on the resolution
        for(int i=0; i < linePoints.Length-1; i++){
            for(float j=resolution; j<=1; j+= resolution){
                Spline.Add(GetSplinePos(j, linePoints[Mathf.Clamp(i-1,0,maxLen)], linePoints[Mathf.Clamp(i,0,maxLen)], linePoints[Mathf.Clamp(i+1,0,maxLen)], linePoints[Mathf.Clamp(i+2,0,maxLen)]));
            }
        }
    }

    /// <summary>
    /// Uses the equation for a catmull-rom spline to create each spline point
    /// </summary>
    /// <param name="t"></param>
    /// <param name="p0"></param>
    /// <param name="p1"></param>
    /// <param name="p2"></param>
    /// <param name="p3"></param>
    /// <returns></returns>
	private Vector3 GetSplinePos(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
	{
		//The coefficients of the cubic polynomial
		Vector3 a = 2f * p1;
		Vector3 b = p2 - p0;
		Vector3 c = 2f * p0 - 5f * p1 + 4f * p2 - p3;
		Vector3 d = -p0 + 3f * p1 - 3f * p2 + p3;

		//The cubic polynomial: a + b * t + c * t^2 + d * t^3
		Vector3 pos = 0.5f * (a + (b * t) + (c * t * t) + (d * t * t * t));

		return pos;
	}

}
