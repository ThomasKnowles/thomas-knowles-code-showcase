﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class GridDrawer : MonoBehaviour {
	[SerializeField] private int width;
	[SerializeField] private int height;
	private LineRenderer lr;
	// Use this for initialization
	void Start () {
		lr = GetComponent<LineRenderer>();
		DrawGrid();
	}

	private void DrawGrid(){
		lr.positionCount = width * height;
		int k = 0;
		int wOffset = width/2;
		int hOffset = height/2;
		for(int i=0; i< width; i++){
			for(int j=0; j<height; j++){
				lr.SetPosition(k, new Vector3(i-wOffset, 0f, j-hOffset));
				k++;
			}
		}
	}
}
