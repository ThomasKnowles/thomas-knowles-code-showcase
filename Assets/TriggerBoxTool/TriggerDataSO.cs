﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TriggerDataSO : ScriptableObject {

    [SerializeField]
    private int count;
    public int Count { get { return count; } }


    [SerializeField]
    public List<Vector3> position;
    [SerializeField]
    public List<Vector3> size;
    [SerializeField]
    public List<Vector3> rotation;
    [SerializeField]
    public List<string> tag;
    [SerializeField]
    public List<string> name;

    void OnEnable(){
		if (position == null || position.Count <= 0) {
            position = new List<Vector3>();
            size = new List<Vector3>();
            rotation = new List<Vector3>();
            tag = new List<string>();
            name = new List<string>();
            count = 0;

            AddBlankValue();
		}
	}

	public void AddBlankValue(){
        Vector3 _position = Vector3.zero;
        Vector3 _size = Vector3.one;
        Vector3 _rotation = Vector3.zero;
        string _tag = "Untagged";
        string _name = "Name";

        position.Add(_position);
        size.Add(_size);
        rotation.Add(_rotation);
        tag.Add(_tag);
        name.Add(_name);

        count = position.Count;
        UnityEditor.EditorUtility.SetDirty(this);
        UnityEditor.AssetDatabase.SaveAssets();
        UnityEditor.AssetDatabase.Refresh();
    }

    public void RemoveValue(int index)
    {
        position.RemoveAt(index);
        size.RemoveAt(index);
        tag.RemoveAt(index);
        name.RemoveAt(index);
        count = position.Count;

        UnityEditor.EditorUtility.SetDirty(this);
        UnityEditor.AssetDatabase.SaveAssets();
        UnityEditor.AssetDatabase.Refresh();
    }

	public void SetPosition(int index, Vector3 _pos){
		if (position[index] != _pos) {
            position[index] = _pos;
			UnityEditor.EditorUtility.SetDirty (this);
		}
	}

	public void SetSize (int index, Vector3 _size){
		if (size[index] != _size) {
            size[index] = _size;
			UnityEditor.EditorUtility.SetDirty (this);
		}
	}

    public void SetRotation(int index, Vector3 _rotation)
    {
        if (rotation[index] != _rotation)
        {
            rotation[index] = _rotation;
            UnityEditor.EditorUtility.SetDirty(this);
        }
    }


    public void SetTag(int index, string _tag){
		if (tag[index] != _tag) {
            tag[index] = _tag;
			UnityEditor.EditorUtility.SetDirty (this);
		}
	}

	public void SetName(int index, string _name){
		if (name[index] != _name) {
            name[index] = _name;
			UnityEditor.EditorUtility.SetDirty (this);
		}
	}
}
