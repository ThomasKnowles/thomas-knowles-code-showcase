﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TriggerData : ISerializationCallbackReceiver {

    public void OnBeforeSerialize() { }

    public void OnAfterDeserialize() { }


    [SerializeField]
	public Vector3 triggerPosition { get; set; }
	[SerializeField]
	public Vector3 triggerSize{ get; set;}
	[SerializeField]
	public string triggerTag { get; set;}
	[SerializeField]
	public string name {get; set; }

	public TriggerData(Vector3 _pos, Vector3 _size, string _tag, string _name){
		triggerPosition = _pos;
		triggerSize = _size;
		triggerTag = _tag;
		name = _name;
	}

}
