﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerBoxDrawer : MonoBehaviour {

    const string path = "Assets/TriggerBoxTool/Data/TriggerBoxData.asset";
    TriggerDataSO triggerData;

    // Use this for initialization
    void Start()
    {
        triggerData = (TriggerDataSO)UnityEditor.AssetDatabase.LoadAssetAtPath(path, typeof(TriggerDataSO));
        if (triggerData != null)
        {
            DrawBoxes();
        }
    }

    void DrawBoxes()
    {
        for(int i = 0; i< triggerData.Count; i++)
        {
            Debug.Log("Box created " + i);
            GameObject g = new GameObject();

            g.transform.position = triggerData.position[i];
            g.transform.rotation = Quaternion.Euler(triggerData.rotation[i]);
            g.name = triggerData.name[i];
            g.tag = triggerData.tag[i];

            Collider col = g.AddComponent<BoxCollider>();
            col.transform.localScale = triggerData.size[i];
        }

    }
	
	
}
