﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class TriggerBoxEditorWindow : EditorWindow {

    [SerializeField]
	TriggerDataSO triggerData;
	bool showAllTriggers = false;
	int currentData = 0;
	const string path = "Assets/TriggerBoxTool/Data/TriggerBoxData.asset";
    const string DRAWER_ASSET_NAME = "trigger_box_drawer";

	[MenuItem("Trigger Box Tool/ToolWindow")]
	public static void InitialiseWindow(){
		TriggerBoxEditorWindow window = GetWindow<TriggerBoxEditorWindow>() as TriggerBoxEditorWindow;
		window.Show ();
	}

	void OnEnable(){
		triggerData = (TriggerDataSO)AssetDatabase.LoadAssetAtPath (path,typeof(TriggerDataSO));
		if (triggerData == null) {
			Debug.Log ("Created trigger box data");
			triggerData = CreateInstance<TriggerDataSO>();
			AssetDatabase.CreateAsset (triggerData, path);
			SaveObject ();
		} else Debug.Log ("Trigger box data loaded");

        GameObject drawer = GameObject.Find(DRAWER_ASSET_NAME);
        if (drawer == null)
        {
            drawer = new GameObject(DRAWER_ASSET_NAME);
            drawer.hideFlags = HideFlags.HideInHierarchy;
            drawer.AddComponent<TriggerBoxDrawer>();
            UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(UnityEditor.SceneManagement.EditorSceneManager.GetActiveScene());
        }

            SceneView.onSceneGUIDelegate += OnSceneGUI;
	}

	void OnDisable(){
		SceneView.onSceneGUIDelegate -= OnSceneGUI;
	}

	void SaveObject(){
		EditorUtility.SetDirty (triggerData);
		AssetDatabase.SaveAssets ();
        AssetDatabase.Refresh();
	}

	void OnGUI(){
		GUILayout.Label("Trigger Box Tool");

		EditorGUILayout.BeginHorizontal ();

		if (GUILayout.Button ("<"))
			MoveSelectedLeft ();

		GUILayout.Label ((currentData+1) + "/" + triggerData.Count.ToString());

		if (GUILayout.Button (">"))
			MoveSelectedRight ();
			
		if (showAllTriggers) {
			if (GUILayout.Button ("Hide All Triggers")) {
				showAllTriggers = false;
			}
		}else {
			if(GUILayout.Button("Show All Triggers")){
				showAllTriggers = true;
			}
		}

		EditorGUILayout.EndHorizontal ();

		if (GUILayout.Button ("Add new trigger")) {
			triggerData.AddBlankValue ();
            currentData = triggerData.Count-1;
		}
        if (GUILayout.Button("Remove current trigger"))
        {
            triggerData.RemoveValue(currentData);
            currentData--;
        }

        EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.BeginVertical ();

		triggerData.SetPosition ( currentData, EditorGUILayout.Vector3Field ("Position", triggerData.position[currentData]));
        triggerData.SetRotation (currentData, EditorGUILayout.Vector3Field("Rotaion", triggerData.rotation[currentData]));
        triggerData.SetSize(currentData, EditorGUILayout.Vector3Field("Size", triggerData.size[currentData]));
        triggerData.SetTag ( currentData, EditorGUILayout.TagField ("Tag", triggerData.tag[currentData]));
		triggerData.SetName (currentData, EditorGUILayout.TextField ("Name", triggerData.name[currentData]));

		EditorGUILayout.EndVertical ();




		EditorGUILayout.BeginVertical ();
		EditorGUILayout.EndVertical ();

		EditorGUILayout.EndHorizontal ();


	}

	void OnSceneGUI(SceneView sceneView){
		if (Event.current.type == EventType.Repaint) {
            Matrix4x4 rotationMatrix = Matrix4x4.TRS(triggerData.position[currentData], Quaternion.Euler(triggerData.rotation[currentData]), Vector3.one);
            Handles.matrix = rotationMatrix;
			Handles.DrawWireCube (Vector3.zero, triggerData.size[currentData]);
            Handles.matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, Vector3.one); ;
        }

		EditorGUI.BeginChangeCheck();


        switch (Tools.current)
        {
            case Tool.Move:
                DrawMoveHandle();
                break;
            case Tool.Rotate:
                DrawRotateHandle();
                break;
            case Tool.Scale:
                DrawScaleHandle();
                break;
            default:
                DrawMoveHandle();
                break;
        }
	}

    void DrawMoveHandle()
    {
        Vector3 handle = triggerData.position[currentData];
        handle = Handles.PositionHandle(handle, Quaternion.Euler(triggerData.rotation[currentData]));
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(triggerData, "Trigger box position changed.");
            triggerData.position[currentData] = handle;
            Repaint();
        }
    }
    void DrawRotateHandle()
    {
        Quaternion handle = Quaternion.Euler(triggerData.rotation[currentData]);
        handle = Handles.RotationHandle(handle,triggerData.position[currentData]);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(triggerData, "Trigger box rotation changed.");
            triggerData.rotation[currentData] = handle.eulerAngles;
            Repaint();
        }
    }

    void DrawScaleHandle()
    {
        Vector3 handlePos = triggerData.position[currentData];
        Vector3 handleScale = triggerData.size[currentData];
        handleScale = Handles.ScaleHandle(handleScale,handlePos, Quaternion.Euler(triggerData.rotation[currentData]),1f);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(triggerData, "Trigger box scale changed.");
            triggerData.size[currentData] = handleScale;
            Repaint();
        }
    }

	void MoveSelectedLeft(){
		if (currentData > 0) {
			currentData--;
			Repaint ();
			SceneView.RepaintAll ();
		}
	}

	void MoveSelectedRight(){
		if (currentData < triggerData.Count - 1) {
			currentData++;
			Repaint ();
			SceneView.RepaintAll ();
		}
	}


}
